mod api;
mod proxy;

use hyper::service::{make_service_fn, service_fn};
use hyper::Server;
use proxy::hosts::*;
use proxy::server::proxy_request;

use std::convert::Infallible;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::sync::mpsc;

#[derive(Debug)]
pub enum Command {
    OpenPort(UserPort),
}

#[tokio::main]
async fn main() {
    let (cmd_sender, mut cmd_receiver) = mpsc::channel::<Command>(10);

    let t1_sender = cmd_sender.clone();
    tokio::spawn(async move {
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        t1_sender
            .send(Command::OpenPort(UserPort::new(None, PortType::Rotative)))
            .await
            .unwrap();
    });

    let make_api_svc = make_service_fn(move |_: &hyper::server::conn::AddrStream| {
        let api_cmd_sender = Arc::new(cmd_sender.clone());
        let api_svc =
            service_fn(move |req| api::server::process_api_req(req, Arc::clone(&api_cmd_sender)));
        async move { Ok::<_, Infallible>(api_svc) }
    });
    tokio::spawn(async move {
        let addr = SocketAddr::from(([127, 0, 0, 1], 6660));
        println!("Starting API server at port 6660");
        let _ = Server::bind(&addr).serve(make_api_svc).await;
    });

    while let Some(cmd) = cmd_receiver.recv().await {
        match cmd {
            Command::OpenPort(user_port) => {
                let addr = SocketAddr::from(([127, 0, 0, 1], user_port.port));
                println!("Starting {:?} Port {}", user_port.port_type, user_port.port);
                let make_svc = make_service_fn(move |_: &hyper::server::conn::AddrStream| {
                    let up = user_port.clone();
                    let svc = service_fn(move |req| proxy_request(req, up.clone()));
                    async move { Ok::<_, Infallible>(svc) }
                });
                tokio::spawn(async move { Server::bind(&addr).serve(make_svc).await });
            }
        }
    }
}
