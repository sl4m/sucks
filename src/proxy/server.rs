use std::convert::Infallible;

use crate::proxy::hosts::*;
use hyper::upgrade::Upgraded;
use hyper::{Body, Method, Request, Response, StatusCode};
use tokio::io;

use tokio_socks::tcp::Socks5Stream;

pub async fn proxy_request(
    req: Request<Body>,
    user_port: UserPort,
) -> Result<Response<Body>, Infallible> {
    let peer = match user_port.port_type {
        PortType::Persistent(peer) => peer,
        PortType::Rotative => {
            let Some(peer) = get_peer(None) else {
                return Ok(ProxyError::NoPeer.into());
            };
            peer
        }
        PortType::RegionRotative(region) => {
            let Some(peer) = get_peer(Some(&region)) else {
                return Ok(ProxyError::NoPeer.into());
            };
            peer
        }
    };

    let Some(addr) = req
        .uri()
        .authority()
        .and_then(|auth| Some(auth.to_string()))
    else {
        return Ok(ProxyError::InvalidTargetAddr.into());
    };

    if req.method() == Method::CONNECT {
        tokio::task::spawn(async move {
            match hyper::upgrade::on(req).await {
                Ok(upgraded) => {
                    if let Err(e) =
                        tunnel(upgraded, &addr, format!("{}:{}", peer.host, peer.port)).await
                    {
                        eprintln!("server io error: {}", e);
                    }
                }
                Err(e) => {
                    eprintln!("upgrade error: {}", e);
                }
            }
        });
        return Ok(Response::new("".into()));
    } else {
        let proxy = reqwest::Proxy::all(format!("socks5://{}:{}", peer.host, peer.port)).unwrap();
        let client = reqwest::Client::builder().proxy(proxy).build().unwrap();
        println!("Proxying {:?} through {:?}", req.uri(), peer.host);

        let mut resp_builder = Response::builder();
        let mut resp_body = String::from("");
        match client.get(req.uri().to_string()).send().await {
            Ok(resp) => {
                resp_body = resp.text().await.unwrap();
            }
            Err(_) => {
                resp_builder = resp_builder.status(502);
            }
        }

        Ok(resp_builder.body(resp_body.into()).unwrap())
    }
}

async fn tunnel(upgraded: Upgraded, addr: &str, proxy_addr: String) -> Result<(), ProxyError> {
    let mut socks_stream = Socks5Stream::connect(proxy_addr.as_str(), addr)
        .await
        .map_err(ProxyError::PeerConnect)?;

    let mut u = upgraded;
    io::copy_bidirectional(&mut u, &mut socks_stream)
        .await
        .map_err(ProxyError::UpgradeError)?;
    Ok(())
}
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ProxyError {
    #[error("No active peer found for selected region")]
    NoPeer,
    #[error("Upstream replied with empty body")]
    EmptyResponse,
    #[error("No reply from upstream server")]
    NoResponse,
    #[error("Invalid target Host address")]
    InvalidTargetAddr,
    #[error("Connection to peer failed")]
    PeerConnect(#[from] tokio_socks::Error),
    #[error("Failed Upgrading Connection")]
    UpgradeError(#[from] std::io::Error),
    #[error("Failed creating a client for the socks connection")]
    ReqwestError(#[from] reqwest::Error),
}

impl From<ProxyError> for Response<Body> {
    fn from(value: ProxyError) -> Self {
        return Response::builder()
            .status(StatusCode::BAD_REQUEST)
            .body(Body::from(value.to_string()))
            .unwrap();
    }
}
