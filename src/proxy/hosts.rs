use rand::seq::IteratorRandom;
use rand::{rngs::SmallRng, Rng, SeedableRng};
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs::File;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Region {
    pub country: String,      // Czech -> cz
    pub city: Option<String>, // Prague -> pra
}
impl PartialEq for Region {
    fn eq(&self, other: &Self) -> bool {
        match self.city {
            Some(ref city) => {
                let Some(other_city) = other.city.as_ref() else {
                    return false;
                };
                self.country.to_lowercase() == other.country.to_lowercase()
                    && city.to_lowercase() == other_city.to_lowercase()
            }
            None => self.country.to_lowercase() == other.country.to_lowercase(),
        }
    }
}
impl Eq for Region {}

#[derive(Deserialize, Clone, Debug)]
pub struct Peer {
    pub host: String,
    pub port: u16,
    pub protocol: Protocol,
    pub region: Region,
    pub provider: Option<String>,
    pub ptype: Option<String>,
}

#[derive(Deserialize, Clone, Debug)]
pub enum Protocol {
    Socks5,
    Http,
}
pub fn get_peer(region: Option<&Region>) -> Option<Peer> {
    let file: File = File::open("peers.json").expect("opening peers.json");
    let mut rng = SmallRng::from_entropy();

    let peers = serde_json::from_reader::<_, Vec<Peer>>(file).expect("reading peers.json");
    if let Some(region) = region {
        peers
            .into_iter()
            .filter(|peer| region == &peer.region)
            .choose(&mut rng)
    } else {
        // TODO cache the RNG
        let ri = rng.gen_range(0..peers.len());
        peers.into_iter().nth(ri)
    }
}

#[derive(Debug, Clone)]
pub struct UserPort {
    pub port: u16,
    pub port_type: PortType,
}
impl UserPort {
    pub fn new(port: Option<u16>, port_type: PortType) -> Self {
        Self {
            port: port.unwrap_or(27015),
            port_type,
        }
    }
}

#[derive(Debug, Clone)]
pub enum PortType {
    Rotative,
    RegionRotative(Region),
    Persistent(Peer),
}
