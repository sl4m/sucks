use crate::proxy::hosts::Region;
use crate::proxy::hosts::*;
use crate::Command;
use base64;
use hyper::{Body, Method, Request, Response, StatusCode};
use rand::{rngs::SmallRng, Rng, SeedableRng};
use std::collections::HashMap;
use std::convert::Infallible;
use std::sync::Arc;
use tokio::sync::mpsc::Sender;

pub async fn process_api_req(
    req: Request<Body>,
    cmd_sender: Arc<Sender<Command>>,
) -> Result<Response<Body>, Infallible> {
    // Create a UserPort for type Persistence
    // select peer from country/city in username
    // Send command to start a listener with the above user port
    let Some(username) = parse_username(&req) else {
        return Ok(ApiError::Username.into());
    };
    let Some(region) = get_region(&username) else {
        return Ok(ApiError::PeerNotFound.into());
    };

    let mut rng = SmallRng::from_entropy();
    let rport = rng.gen_range(10000..65535);

    let Some(peer) = get_peer(Some(&region)) else {
        return Ok(ApiError::PeerNotFound.into());
    };
    let user_port = match req.method() {
        // indicates PortType::RegionRotative
        &Method::GET => UserPort {
            port: rport.clone(),
            port_type: PortType::RegionRotative(region.clone()),
        },
        // indicates PortType::Persistent
        &Method::POST => UserPort {
            port: rport.clone(),
            port_type: PortType::Persistent(peer),
        },
        _ => return Ok(ApiError::MethodNotAllowed.into()),
    };

    cmd_sender
        .send(Command::OpenPort(user_port.clone()))
        .await
        .expect("Failed to send Persistent UserPort");

    return Ok(Response::new(Body::from(
        serde_json::json!(
        {
            "peer": region,
            "port": user_port.port
        })
        .to_string(),
    )));
}

fn parse_username(req: &Request<Body>) -> Option<String> {
    let req_headers = req.headers();
    let auth_header = req_headers
        .get("Proxy-Authorization")
        .or(req_headers.get("Authorization"))?;

    let Some((_, up_b64)) = auth_header.to_str().unwrap_or("").split_once(' ') else {
        return None;
    };

    let up = String::from_utf8(base64::decode(up_b64).unwrap_or(vec![])).unwrap_or("".to_string());

    let Some((user_name, _)) = up.split_once(':') else {
        return None;
    };
    Some(user_name.to_owned())
}
pub fn get_region(username: &str) -> Option<Region> {
    if username.to_lowercase().starts_with("sucks-") {
        let mut settings: HashMap<&str, &str> = HashMap::new();

        let settings_order = ["country", "city"];
        for (i, setting) in username.split('-').skip(1).enumerate() {
            if i < settings_order.len() {
                settings.insert(settings_order[i], setting);
            }
        }

        let Some(&chosen_country) = settings.get("country") else {
            return None;
        };

        let region = Region {
            country: chosen_country.to_owned(),
            city: settings.get("city").and_then(|s| Some(s.to_string())),
        };
        return Some(region);
    }
    None
}

use thiserror::Error;
#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed Parsing Username\nExample Username: COUNTRY-city\n")]
    Username,
    #[error("Peer Not Found")]
    PeerNotFound,
    #[error("")]
    MethodNotAllowed,
    #[error("Invalid Target Address")]
    InvalidTargetAddr,
}

impl From<ApiError> for Response<Body> {
    fn from(err: ApiError) -> Self {
        match err {
            ApiError::MethodNotAllowed => {
                return Response::builder()
                    .status(StatusCode::METHOD_NOT_ALLOWED)
                    .body(Body::from(err.to_string()))
                    .expect("api method not found response")
            }
            _ => {
                return Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(Body::from(err.to_string()))
                    .expect("api bad request response")
            }
        }
    }
}
